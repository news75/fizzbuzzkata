﻿using NUnit.Framework;

namespace FizzBuzzKataProject
{
    [TestFixture]
    public class UnitTest1
    {
        private Calcolatore _calcolatore;

        [SetUp]
        public void SetUp()
        {
            _calcolatore = new Calcolatore();
        }

        [Test]
        public void TestSePassoUnoTornaUno()
        {
            var risultato = _calcolatore.Calcola(1);

            Assert.AreEqual("1", risultato);
        }

        [Test]
        public void Provoilmodulo()
        {
            var risultato = 6%3;

            Assert.AreEqual(0, risultato);
        }

        [Test]
        public void TestSeDividoTreTornaFizz()
        {
            var risultato = _calcolatore.Calcola(6);

            Assert.AreEqual("Fizz", risultato);
        }

        [Test]
        public void TestSeDividoCinqueTornaBuzz()
        {
            var risultato = _calcolatore.Calcola(10);

            Assert.AreEqual("Buzz", risultato);
        }
    }
}
