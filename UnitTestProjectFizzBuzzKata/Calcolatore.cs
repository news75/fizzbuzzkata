namespace FizzBuzzKataProject
{
    public class Calcolatore
    {
        public string Calcola(int numeroDaConvertire)
        {
            if (numeroDaConvertire%3 == 0) return "Fizz";
            if (numeroDaConvertire%5 == 0) return "Buzz";
            return numeroDaConvertire.ToString();
        }
    }
}